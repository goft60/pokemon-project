<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Nouveau dresseur'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Associer avec un pokémon'), ['controller' => 'DresseursPokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<?= $this->assign('Dresseurs', $titleContent); ?>

<div class="dresseurs view large-9 medium-8 columns content">
    <h3><?= h($dresseur->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Pseudo') ?></th>
            <td><?= h($dresseur->nickname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseur->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Créé le') ?></th>
            <td><?= h($dresseur->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modifié le') ?></th>
            <td><?= h($dresseur->modified) ?></td>
        </tr>
    </table>
</div>
