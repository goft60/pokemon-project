<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Nouveau dresseur'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Associer avec un pokémon'), ['controller' => 'DresseursPokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseurs form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseur) ?>
    <fieldset>
        <legend><?= __('Ajouter un dresseur') ?></legend>
        <?php
            echo $this->Form->control('nickname');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Ajouter')) ?>
    <?= $this->Form->end() ?>
</div>
