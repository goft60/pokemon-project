<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>

<div class="fights view large-9 medium-8 columns content">
    <h3><?= h($poke->name)?></h3>
    <table class="vertical-table">
        <tr>
            <th></th>
            <th scope="row"><?= __('Pokémon') ?></th>
            <th scope="row"><?= __('N°Pokédex') ?></th>
            <th scope="row"><?= __('Créé le') ?></th>
            <th scope="row"><?= __('Modifié le') ?></th>
            <th scope="row"><?= __('Santé') ?></th>
            <th scope="row"><?= __('Attaque') ?></th>
            <th scope="row"><?= __('Défense') ?></th>
        </tr>
        <tr>       
                <td><img src="<?= h($poke->sprite) ?>" scale="80%"/></td>
                <td><?= h($poke->name) ?></td>
                <td><?= $this->Number->format($poke->pokedex_number) ?></td>
                <td><?= h($poke->created) ?></td>
                <td><?= h($poke->modified) ?></td>
                <td><?= $this->Number->format($poke->health) ?></td>
                <td><?= $this->Number->format($poke->attack) ?></td>
                <td><?= $this->Number->format($poke->defense) ?></td>
        </tr>
        
    </table>
</div>
</div>
