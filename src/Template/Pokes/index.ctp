<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke[]|\Cake\Collection\CollectionInterface $pokes
 */
?>
    <h2><?= __('Pokémons') ?></h3>
    <div class="container">
        <?php foreach ($pokes as $poke): ?>
            <div class="card" style="width: 18rem; display:inline-block;">
            <img src= "<?= h($poke->sprite) ?>" class="card-img-top" alt="<?= h($poke->name) ?>">
                <div class="card-body">
                    <h4 class="card-title"><?= $this->Html->link(__(h($poke->name)), ['action' => 'view/'.$poke->pokedex_number]) ?></h5>
                    <p class="card-text">Santé : <?= $this->Number->format($poke->health) ?></p>
                    <p class="card-text">Attaque : <?= $this->Number->format($poke->attack) ?></p>
                    <p class="card-text">Défense : <?= $this->Number->format($poke->defense) ?></p>
                </div>
            </div>
        
        <?php endforeach; ?>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} sur {{pages}}, {{current}} éléments parmi les {{count}} totaux')]) ?></p>
    </div>
</div>
