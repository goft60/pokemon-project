<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(__('Delete Fight'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fights'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fight'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<?= $this->assign('Fights', $titleContent); ?>
<div class="fights view large-9 medium-8 columns content">
    <h3><?= h($fight->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ID du gagnant') ?></th>
            <td><?= h($fight->winner_dresseur_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($fight->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID Dresseur 1') ?></th>
            <td><?= $this->Number->format($fight->first_dresseur_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID Dresseur 2') ?></th>
            <td><?= $this->Number->format($fight->second_dresseur_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Créé le') ?></th>
            <td><?= h($fight->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modifié le') ?></th>
            <td><?= h($fight->modified) ?></td>
        </tr>
    </table>
</div>
