<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke $dresseursPoke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dresseursPoke->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dresseursPoke->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Associations existantes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nouvelle association'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokémons'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="dresseursPokes form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseursPoke) ?>
    <fieldset>
        <legend><?= __('Edit Dresseurs Poke') ?></legend>
        <?php
            echo $this->Form->control('dresseur_id', ['options' => $dresseurs]);
            echo $this->Form->control('poke_id', ['options' => $pokes]);
            echo $this->Form->control('favorite');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Envoyer')) ?>
    <?= $this->Form->end() ?>
</div>
