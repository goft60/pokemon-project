<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke[]|\Cake\Collection\CollectionInterface $dresseursPokes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Associations existantes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nouvelle association'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokémons'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
    </ul>
</nav>

<div class="dresseursPokes index large-9 medium-8 columns content">
    <h3><?= __('Dresseurs Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ID Dresseur') ?></th>
                <th scope="col"><?= $this->Paginator->sort('N°Pokédex pokémon') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Favori') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseursPokes as $dresseursPoke): ?>
            <tr>
                <td><?= $this->Number->format($dresseursPoke->id) ?></td>
                <td><?= $dresseursPoke->has('dresseur') ? $this->Html->link($dresseursPoke->dresseur->id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseursPoke->dresseur->id]) : '' ?></td>
                <td><?= $dresseursPoke->has('poke') ? $this->Html->link($dresseursPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseursPoke->poke->id]) : '' ?></td>
                <td><?= h($dresseursPoke->favorite) ?></td>
                <td><?= h($dresseursPoke->created) ?></td>
                <td><?= h($dresseursPoke->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseursPoke->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseursPoke->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseursPoke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseursPoke->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} sur {{pages}}, {{current}} éléments des {{count}} totaux')]) ?></p>
    </div>
</div>
