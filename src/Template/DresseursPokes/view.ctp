<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke $dresseursPoke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Associations existantes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nouvelle association'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokémons'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
    </ul>
</nav>
<?= $this->assign('Dresseurs - Pokémons', $titleContent); ?>
<div class="dresseursPokes view large-9 medium-8 columns content">
    <h3><?= h($dresseursPoke->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseursPoke->has('dresseur') ? $this->Html->link($dresseursPoke->dresseur->id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseursPoke->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pokémon') ?></th>
            <td><?= $dresseursPoke->has('poke') ? $this->Html->link($dresseursPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseursPoke->poke->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($dresseursPoke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Créé le') ?></th>
            <td><?= h($dresseursPoke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modifié le') ?></th>
            <td><?= h($dresseursPoke->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Favori') ?></th>
            <td><?= $dresseursPoke->favorite ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
