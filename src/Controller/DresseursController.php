<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dresseurs Controller
 *
 * @property \App\Model\Table\DresseursTable $Dresseurs
 *
 * @method \App\Model\Entity\Dresseur[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DresseursController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $dresseurs = $this->paginate($this->Dresseurs);

        $this->set(compact('dresseurs'));
    }

    /**
     * View method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dresseur = $this->Dresseurs->get($id, [
            'contain' => ['DresseurPokemons']
        ]);

        $this->set('dresseur', $dresseur);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dresseur = $this->Dresseurs->newEntity();
        if ($this->request->is('post')) {
            $dresseur = $this->Dresseurs->patchEntity($dresseur, $this->request->getData());
            if ($this->Dresseurs->save($dresseur)) {
                $this->Flash->success(__('Le dresseur a bien été ajouté !'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Une erreur est survenue dans l'enregistrement du dresseur !"));
        }
        $this->set(compact('dresseur'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dresseur = $this->Dresseurs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dresseur = $this->Dresseurs->patchEntity($dresseur, $this->request->getData());
            if ($this->Dresseurs->save($dresseur)) {
                $this->Flash->success(__('Le dresseur a été modifié !'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Impossible de modifier le dresseur !'));
        }
        $this->set(compact('dresseur'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dresseur = $this->Dresseurs->get($id);
        if ($this->Dresseurs->delete($dresseur)) {
            $this->Flash->success(__('Le dresseur a été correctement supprimé'));
        } else {
            $this->Flash->error(__('Impossible du supprimer le dresseur !'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
