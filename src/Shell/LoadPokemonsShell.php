<?php
namespace App\Shell;

use Cake\Console\Shell;

use Cake\Http\Client;
use Cake\Log\Log;


/**
 * LoadPokemons shell command.
 */
class LoadPokemonsShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
        $this->out('coucou');
        $this->loadModel('Pokes');
        $this->loadModel('Dresseurpokes');

        $http = new Client();
        for($i = 803; $i < 808; $i++) // 807 last id
        {
            $response = $http->get("https://pokeapi.co/api/v2/pokemon/${i}");
            $json = $response->getJson();
            $poke = $this->Pokes->newEntity();
            $poke->pokedex_number = $json['id'];
            $poke->name = $json['name'];
            $poke->defense = $json['stats'][3]['base_stat'];
            $poke->attack = $json['stats'][4]['base_stat'];
            $poke->health = $json['stats'][5]['base_stat'];
            $poke->sprite = ($json['sprites']['front_default'] == null ? "" : $json['sprites']['front_default']);
            //$poke = $this->Pokes->patchEntity($poke,$json, ['pokedex_number' => $json['id'], 'name' => $json['name']]);
            if ($this->Pokes->save($poke)) {
                    $this->out("Pokémon ${json['name']} ${i}) - HP : " . $poke->health . "-  has been successfully saved");
                    
            }
        }  
        return header("Location:index");
     }
}
